//
//  DeviceOrientationSingleton.m
//  SampleApp
//
//  Created by Luciana Silva Daré on 28/03/19.
//

#import "DeviceOrientationSingleton.h"

@implementation DeviceOrientationSingleton

#pragma mark Singleton Methods

+ (id)sharedManager {
    static DeviceOrientationSingleton *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
        deviceOrientation = UIDeviceOrientationUnknown;
        
        [self registerOrientationNotifications];
    }
    return self;
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
}

- (void) registerOrientationNotifications {
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter]
     addObserver: self
     selector: @selector(orientationDidChanged:)
     name: UIDeviceOrientationDidChangeNotification
     object: [UIDevice currentDevice]];
}

- (void) orientationDidChanged:(NSNotification *)note
{
    UIDevice * device = note.object;
    if (device.orientation != UIDeviceOrientationFaceUp && device.orientation != UIDeviceOrientationFaceDown) {
        deviceOrientation = device.orientation;
    }
}

- (void) resumeOrientationUpdates {
    [self registerOrientationNotifications];
}

- (void) stopOrientationUpdates {
    [[NSNotificationCenter defaultCenter]
     removeObserver: self
     name: UIDeviceOrientationDidChangeNotification
     object: nil];
}

- (UIInterfaceOrientation) interfaceOrientation {
    switch (deviceOrientation) {
        case UIDeviceOrientationPortrait:
            return UIInterfaceOrientationPortrait;
        case UIDeviceOrientationPortraitUpsideDown:
            return UIInterfaceOrientationPortraitUpsideDown;
        case UIDeviceOrientationLandscapeRight:
            return UIInterfaceOrientationLandscapeLeft;
        case UIDeviceOrientationLandscapeLeft:
            return UIInterfaceOrientationLandscapeRight;
        default:
            return UIInterfaceOrientationUnknown;
    }
}

@end

#import <UIKit/UIKit.h>
#import <IDV_Doc/CaptureInterface.h>
#import <Cordova/CDVPlugin.h>

@interface McidSDKIntegration : UIViewController <CaptureDelegate>
- (IBAction)cancelCapture:(id)sender;
@property (nonatomic, strong) NSString* callbackId;
@property (nonatomic, weak) id <CDVCommandDelegate> commandDelegate;
@property NSNumber* detectZone1;
@property NSNumber* detectZone2;
@property NSNumber* edgeDetectionTimeout;
@property NSNumber* autoSnapshot;
@property NSNumber* autoCropping;
@property NSNumber* uIElementsForStep;
@property NSNumber* QACheckResultTimeout;
@property NSNumber* SecondPageTimeout;
@property NSNumber* captureDoc;
@end

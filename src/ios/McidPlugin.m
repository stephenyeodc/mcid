#import "McidPlugin.h"
#import "DocViewController.h"

@interface McidPlugin()

@property(nonatomic, strong) DocViewController* docController;

@end

@implementation McidPlugin

int docChoice = 0;
int detectZone1;
float detectZone2;
int edgeDetectionTimeout;
Boolean autoSnapshot;
Boolean autoCropping;
NSString* uIElementsForStep;
int QACheckResultTimeout;
int SecondPageTimeout;

- (void)pluginInitialize {
    
    self.docController = [[DocViewController alloc] init];
}

#pragma mark public

- (void)init:(CDVInvokedUrlCommand*)command {
//    DocViewController *docController = [[DocViewController alloc] init];
    self.docController.callbackId = command.callbackId;
    self.docController.commandDelegate = self.commandDelegate;
    [self.viewController presentViewController:self.docController animated:YES completion:nil];
}

- (void)setDetectionZone:(CDVInvokedUrlCommand*)command{
    self.docController.detectZone1 = [command.arguments objectAtIndex:0];
    self.docController.detectZone2 = [command.arguments objectAtIndex:1];
}
- (void)setCaptureDocuments:(CDVInvokedUrlCommand*)command{
    self.docController.captureDoc = [command.arguments objectAtIndex:0];
}
- (void)setCaptureNewDocument:(CDVInvokedUrlCommand*)command{
    self.docController.width = [command.arguments objectAtIndex:0];
    self.docController.height = [command.arguments objectAtIndex:1];
    self.docController.pages = [command.arguments objectAtIndex:2];
}
- (void)setAutoSnapshot:(CDVInvokedUrlCommand*)command{
    self.docController.autoSnapshot = [command.arguments objectAtIndex:0];
}
- (void)setEdgeDetectionTimeout:(CDVInvokedUrlCommand*)command{
    self.docController.edgeDetectionTimeout = [command.arguments objectAtIndex:0];
}
- (void)setAutocropping:(CDVInvokedUrlCommand*)command{
    self.docController.autoCropping = [command.arguments objectAtIndex:0];
}
- (void)hideUIElementsForStep:(CDVInvokedUrlCommand*)command{
    self.docController.uIElementsForStep = [command.arguments objectAtIndex:0];
}
- (void)setQACheckResultTimeout:(CDVInvokedUrlCommand*)command{
    self.docController.QACheckResultTimeout = [command.arguments objectAtIndex:0];
}
- (void)setSecondPageTimeout:(CDVInvokedUrlCommand*)command{
    self.docController.SecondPageTimeout = [command.arguments objectAtIndex:0];
}

@end

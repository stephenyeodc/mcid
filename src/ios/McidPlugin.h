//
//  McidPlugin.h
//  SampleApp
//
//  Created by Luciana Silva Daré on 28/03/19.
//

#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>

/**
 * GAH cordova plugin wrapping the IDV SDK.
 */
@interface McidPlugin : CDVPlugin

/**
 * Initializes IDV SDK.
 */
- (void)init:(CDVInvokedUrlCommand*)command;
- (void)setDetectionZone:(CDVInvokedUrlCommand*)command;
- (void)setCaptureDocuments:(CDVInvokedUrlCommand*)command;
- (void)setCaptureNewDocument:(CDVInvokedUrlCommand*)command;
- (void)setAutoSnapshot:(CDVInvokedUrlCommand*)command;
- (void)setEdgeDetectionTimeout:(CDVInvokedUrlCommand*)command;
- (void)setAutocropping:(CDVInvokedUrlCommand*)command;
- (void)hideUIElementsForStep:(CDVInvokedUrlCommand*)command;
- (void)setQACheckResultTimeout:(CDVInvokedUrlCommand*)command;
- (void)setSecondPageTimeout:(CDVInvokedUrlCommand*)command;

@end

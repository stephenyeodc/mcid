//
//  Store.m
//  SampleApp
//
//  Created by Luciana Silva Daré on 28/03/19.
//

#import "Store.h"

@implementation Store

+(Store*) getInstance
{
    static Store *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[Store alloc] init];
        //instance.preferences = [NSUserDefaults standardUserDefaults];
    });
    return instance;
}

-(void) addValue:(CaptureError)value forKey:(NSString*)key
{
    [_preferences setInteger:value forKey:key];
    [_preferences synchronize];
}

-(NSInteger) getValueForKey:(NSString*)key
{
    // Due to MKYCError has zero as a valid value,
    // we use the objectForKey which returns nil when a value is not present
    NSInteger value;
    NSNumber* fromStore = [_preferences objectForKey:key];
    if(fromStore == nil)
    {
        value = -1;
    } else {
        value = [fromStore integerValue];
    }
    return value;
}

@end

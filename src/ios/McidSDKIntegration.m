#import "McidSDKIntegration.h"
#import "Store.h"
#import "DeviceOrientationSingleton.h"

@interface McidSDKIntegration ()

//@property (strong, nonatomic) IBOutlet CaptureInterface *camera;
//@property (weak, nonatomic) IBOutlet UIView *resultView;
//@property (weak, nonatomic) IBOutlet UIImageView *docFrontSide;
//@property (weak, nonatomic) IBOutlet UIImageView *docBackSide;
//@property (weak, nonatomic) IBOutlet UIImageView *docPassport;
//@property (weak, nonatomic) IBOutlet UITextView *infoTextView;
//@property (weak, nonatomic) IBOutlet UIButton *cancelCaptureBtn;

@property (strong, nonatomic) IBOutlet CaptureInterface *camera;
@property (weak, nonatomic) IBOutlet UIView *resultView;
@property (weak, nonatomic) IBOutlet UIImageView *docFrontSide;
@property (weak, nonatomic) IBOutlet UIImageView *docBackSide;
@property (weak, nonatomic) IBOutlet UIImageView *docPassport;
@property (weak, nonatomic) IBOutlet UITextView *infoTextView;
@property (weak, nonatomic) IBOutlet UIButton *cancelCaptureBtn;

@end

@implementation McidSDKIntegration

- (void)viewDidLoad {
    [super viewDidLoad];
    self.camera.hidden = false;
    self.resultView.hidden = true;
    
    if(self.detectZone2 != nil && self.detectZone1 != nil)
        [self.camera setDetectionZoneSpace:[self.detectZone1 intValue] andAspectRatio:[self.detectZone2 floatValue]];
    
    if(self.edgeDetectionTimeout != nil)
        [self.camera setEdgeDetectionTimeout:[self.edgeDetectionTimeout intValue]];
    
    if(self.autoSnapshot != nil)
        [self.camera setAutoSnapshot:[self.autoSnapshot boolValue]];
    
    if(self.autoCropping != nil)
        [self.camera setAutoCropping:[self.autoCropping boolValue]];
    
    if(self.uIElementsForStep != nil){
        switch ([self.uIElementsForStep intValue]) {
            case Detecting:
                [self.camera hideUIElementsForStep:Detecting];
                break;
            case Cropping:
                [self.camera hideUIElementsForStep:Cropping];
                break;
            case ResultOK:
                [self.camera hideUIElementsForStep:ResultOK];
                break;
            case ResultKO:
                [self.camera hideUIElementsForStep:ResultKO];
                break;
        }
    }
    
    if(self.QACheckResultTimeout != nil)
        [self.camera setQACheckResultTimeout:[self.QACheckResultTimeout intValue]];
    
    if(self.SecondPageTimeout != nil)
        [self.camera setSecondPageTimeout:[self.SecondPageTimeout intValue]];
    
    if(self.captureDoc != nil){
        switch ([self.captureDoc intValue]) {
            case 0: [self.camera setCaptureDocuments:DocumentModeIdDocument]; break;
            case 1: [self.camera setCaptureDocuments:DocumentModePassport]; break;
            case 2: [self.camera setCaptureDocuments:DocumentModeICAO]; break;
            default: [self.camera setCaptureDocuments:DocumentModeIdDocument]; break;
        }
    }
    
    self.navigationController.navigationBarHidden = YES;
    
    [[UIDevice currentDevice] setValue:[NSNumber numberWithInt: ((DeviceOrientationSingleton*)[DeviceOrientationSingleton sharedManager]).interfaceOrientation]
                                forKey:@"orientation"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    _camera = nil;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // start the doc capture
    
    [self.camera initWithCompletion:^(BOOL isCompleted, int errorCode) {
        if(isCompleted) {
            double delayInSeconds = 4.0f;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [self.camera start:self];
            });
        } else {
            NSLog(@"Error on init: %i", errorCode);
        }
    }];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [self.camera stop];
    
}

-(BOOL)shouldAutorotate
{
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return (UIInterfaceOrientationMaskLandscapeRight);
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (IBAction)cancelAction:(id)sender {
    
    // cancel the capture before dismiss the viewcontroller
    [self.camera stop];
    [self dismissViewControllerAnimated:true completion:nil];
}

#pragma mark - CaptureDelegate
- (void) onSuccess:(NSData*)p_side1 :(NSData*)p_side2 :(NSDictionary*)p_metaData
{
    // do anything after the SDK makes a succesfull capture
    
    self.camera.hidden = true;
    self.resultView.hidden = false;
    
    if (p_side2) {
        
        self.docPassport.hidden = true;
        
        self.docFrontSide.hidden = false;
        self.docFrontSide.image = p_side1 ? [UIImage imageWithData:p_side1] : nil;
        
        self.docBackSide.hidden = false;
        self.docBackSide.image = [UIImage imageWithData:p_side2];
        
    } else {
        
        self.docFrontSide.hidden = true;
        
        self.docBackSide.hidden = true;
        
        self.docPassport.hidden = false;
        self.docPassport.image = p_side1 ? [UIImage imageWithData:p_side1] : nil;
    }
    
    NSString *text = @"---------\n";
    
    if (p_metaData != NULL) {
        NSArray *keys = [p_metaData allKeys];
        text = [text stringByAppendingString: @"Document meta data : \n"];
        for (NSString *key in keys) {
            text = [text stringByAppendingString: [NSString stringWithFormat:@"  - %@ = %@\n", key, [p_metaData objectForKey:key]]];
        }
    }
    
    self.infoTextView.text = text;
    self.navigationController.navigationBarHidden = NO;
    self.cancelCaptureBtn.hidden = YES;
    
    NSString *side1_base64String = [p_side1 base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    NSString *side2_base64String = [p_side2 base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    id objects[] = {side1_base64String, side2_base64String, text};
    id keys[] = {@"side_1",@"side_2",@"metadata"};
    
    NSDictionary *dicResult = [[NSDictionary alloc] initWithObjects:objects forKeys:keys count:3];
    
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:dicResult];
        [self.commandDelegate sendPluginResult:result callbackId:self.callbackId];
    }];
    
    [self.presentingViewController dismissViewControllerAnimated:true completion:nil];
    
}

- (void)onError:(CaptureError)p_failureCode
{
    [[Store getInstance] addValue:p_failureCode forKey:@"IDVDOC_SDK_ERROR_CODE"];
    
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"IDVDOC_SDK_ERROR"];
        [self.commandDelegate sendPluginResult:result callbackId:self.callbackId];
    }];
    
    [self.presentingViewController dismissViewControllerAnimated:true completion:nil];
    
}


- (IBAction)cancelCapture:(id)sender {
    
    [self dismissViewControllerAnimated:true completion:nil];
}

@end


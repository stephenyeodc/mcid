var exec = require('cordova/exec');

var PLUGIN_NAME = 'McidPlugin';

function McidPlugin() {
}

McidPlugin.prototype.init = function(callback, errorCallback) {
                                         exec(callback, errorCallback, PLUGIN_NAME, 'init', []);
                                         };

McidPlugin.prototype.setDetectionZone = function(var1, var2 , callback, errorCallback) {
                                         exec(callback, errorCallback, PLUGIN_NAME, 'setDetectionZone', [var1, var2]);
                                         };

McidPlugin.prototype.setCaptureDocuments = function(docChoice, callback, errorCallback) {
                                         exec(callback, errorCallback, PLUGIN_NAME, 'setCaptureDocuments', [docChoice]);
                                         };

McidPlugin.prototype.setCaptureNewDocument = function(width, height, pages, callback, errorCallback) {
                                         exec(callback, errorCallback, PLUGIN_NAME, 'setCaptureNewDocument', [width, height, pages]);
                                         };

McidPlugin.prototype.setAutoSnapshot = function(autoSnapshot, callback, errorCallback) {
                                         exec(callback, errorCallback, PLUGIN_NAME, 'setAutoSnapshot', [autoSnapshot]);
                                         };

McidPlugin.prototype.setEdgeDetectionTimeout  = function(timeout, callback, errorCallback) {
                                         exec(callback, errorCallback, PLUGIN_NAME, 'setEdgeDetectionTimeout', [timeout]);
                                         };

McidPlugin.prototype.setAutocropping = function(autocropping, callback, errorCallback) {
                                         exec(callback, errorCallback, PLUGIN_NAME, 'setAutocropping', [autocropping]);
                                         };

McidPlugin.prototype.hideUIElementsForStep = function(step, callback, errorCallback) {
                                         exec(callback, errorCallback, PLUGIN_NAME, 'hideUIElementsForStep', [step]);
                                         };
McidPlugin.prototype.setQACheckResultTimeout = function(timeout, callback, errorCallback) {
                                         exec(callback, errorCallback, PLUGIN_NAME, 'setQACheckResultTimeout', [timeout]);
                                         };
McidPlugin.prototype.setSecondPageTimeout = function(timeout, callback, errorCallback) {
                                         exec(callback, errorCallback, PLUGIN_NAME, 'setSecondPageTimeout', [timeout]);
                                         };



var McidPlugin = new McidPlugin();
module.exports = McidPlugin;
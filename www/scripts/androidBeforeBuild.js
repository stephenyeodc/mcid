var fsCallback = function (err) {
    if (err) {
        console.error("Failed to create directory or file.");
        throw err;
    }
}
module.exports = function(context) {
    var fs = require('fs');
	var fsEx = require("fs.extra");
    var path = require('path');
    var config_xml = path.join(context.opts.projectRoot, 'config.xml');
    var et = context.requireCordovaModule('elementtree');
    // var et = require('elementtree');

    var data = fs.readFileSync(config_xml).toString();
    var etree = et.parse(data);
    var app_id = etree.getroot().attrib.id;

    console.log("Before build js for Android");

    //Get Application id
    console.log(etree.getroot().attrib.id);

    var file = fs.readFileSync('plugins/cordova.plugin.mcid/src/android/CaptureDocActivity.java', 'utf8');

    var result = file.replace(/applicationId/g, app_id);

    fs.writeFileSync('plugins/cordova.plugin.mcid/src/android/CaptureDocActivity.java', result);

    console.log('Application id replacement in CaptureDocActivity done!');

    // Copy CaptureDocActivity.java
    fsEx.copy('plugins/cordova.plugin.mcid/src/android/CaptureDocActivity.java', 'platforms/android/app/src/main/java/cordova/plugin/mcid/CaptureDocActivity.java', { replace: true }, fsCallback);

    // Copy activity_capture_doc.xml to res/layout
    fsEx.mkdirp('platforms/android/app/src/main/res/layout', fsCallback);
    fsEx.copy('plugins/cordova.plugin.mcid/src/res/layout/activity_capture_doc.xml', 'platforms/android/app/src/main/res/layout/activity_capture_doc.xml', { replace: true }, fsCallback);

    console.log('Customization done for Android');

};